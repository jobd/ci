package duration

import (
	"errors"
	"fmt"
	"testing"
	"time"
)

func TestParse(t *testing.T) {
	tests := []struct {
		input    string
		expected float64
		err      error
	}{
		{"1:20", minute + 20*second, nil},
		{"1:20.51", minute + 20.51*second, nil},
		{"4:01:01", 4*hour + minute + second, nil},
		{"3 mins 4 sec", 3*minute + 4*second, nil},
		{"3 Mins 4 Sec", 3*minute + 4*second, nil},
		{"three mins four sec", 3*minute + 4*second, nil},
		{"2 hrs 20 min", 2*hour + 20*minute, nil},
		{"2h20min", 2*hour + 20*minute, nil},
		{"6 mos 1 day", 6*30*24*hour + day, nil},
		{"1 year 6 mos 1 day", year + 6*month + day, nil},
		{"2.5 hrs", 2*hour + 30*minute, nil},
		{"47 yrs 6 mos and 4.5d", 47*year + 6*month + 4*day + 12*hour, nil},
		{"two hours and twenty minutes", 2*hour + 20*minute, nil},
		{"four hours and forty minutes", 4*hour + 40*minute, nil},
		{"3 weeks and, 2 days", 3*week + 2*day, nil},
		{"3 weeks, plus 2 days", 3*week + 2*day, nil},
		{"3 weeks with 2 days", 3*week + 2*day, nil},
		{"1 month", month, nil},
		{"2 months", 2 * month, nil},
		{"18 months", 18 * month, nil},
		{"1 year 6 months", year + 6*month, nil},
		{"day", day, nil},
		{"minute 30s", minute + 30*second, nil},
		{"sixteen hundred seconds", 16 * 100 * second, nil},
		{"thousand million seconds and 26 seconds", 16666667.1 * minute, nil},
		{"1 minute and :10", 0, fmt.Errorf("%w %v", ErrUnexpectedSegmentedTimeFormat, `(offset: 13) ("1 minute and :10" <)`)},
		{"1 minute and 10:10:10:10", 0, fmt.Errorf("%w %v", ErrUnexpectedSegmentedTimeFormat, `(offset: 13) ("1 minute and 10:10:10:10" <)`)},
		{"a", 0, fmt.Errorf("%w %v", ErrUnexpectedToken, `(offset: 0) ("a" <)`)},
		{"one hundred and 80 buckets", 0, fmt.Errorf("%w %v", ErrUnexpectedToken, `(offset: 19) ("one hundred and 80 buckets" <)`)},
	}

	for _, tc := range tests {
		d, err := Parse(tc.input)
		if tc.err == nil && err != nil {
			t.Errorf("%s: unexpected error: %w", tc.input, err)
			continue
		}

		if err != nil && (errors.Unwrap(err) != errors.Unwrap(tc.err) || err.Error() != tc.err.Error()) {
			t.Errorf("%s: expected error %v, got %v", tc.input, tc.err, err)
		}

		expected := time.Duration(tc.expected * float64(time.Second))
		if d != expected {
			t.Errorf("%s: expected %s, got %s", tc.input, expected, d)
		}
	}
}

func BenchmarkDuration(b *testing.B) {
	input := "one hundred minutes and ten thousand seconds"

	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		_, err := Parse(input)
		if err != nil {
			b.Error(err)
		}
	}
}
