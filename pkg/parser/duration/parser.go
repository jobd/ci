package duration

import (
	"errors"
	"fmt"
	"io"
	"strconv"
	"strings"
	"time"

	"gitlab.com/jobd/ci/pkg/parser/duration/lexer"
)

const (
	second = float64(1)
	minute = float64(60)
	hour   = float64(60 * 60)
	day    = 24 * hour
	week   = 7 * day
	month  = 30 * day
	year   = 8766 * hour // 365.25 days in a year
)

var durationMap = map[string]float64{
	"y":       year,
	"w":       week,
	"d":       day,
	"h":       hour,
	"m":       minute,
	"s":       second,
	"yr":      year,
	"mo":      month,
	"wk":      week,
	"day":     day,
	"hr":      hour,
	"min":     minute,
	"sec":     second,
	"yrs":     year,
	"mos":     month,
	"wks":     week,
	"days":    day,
	"hrs":     hour,
	"mins":    minute,
	"secs":    second,
	"year":    year,
	"month":   month,
	"week":    week,
	"hour":    hour,
	"minute":  minute,
	"second":  second,
	"years":   year,
	"months":  month,
	"weeks":   week,
	"hours":   hour,
	"minutes": minute,
	"seconds": second,

	"one":   1,
	"two":   2,
	"three": 3,
	"four":  4,
	"five":  5,
	"six":   6,
	"seven": 7,
	"eight": 8,
	"nine":  9,

	"zero":      0,
	"ten":       10,
	"eleven":    11,
	"twelve":    12,
	"thirteen":  13,
	"fourteen":  14,
	"fifteen":   15,
	"sixteen":   16,
	"seventeen": 17,
	"nineteen":  19,

	"twenty":  20,
	"thirty":  30,
	"forty":   40,
	"fifty":   50,
	"sixty":   60,
	"seventy": 70,
	"eighty":  80,
	"ninety":  90,

	"hundred":  100,
	"thousand": 1000,
	"million":  1000000,
	"billion":  100000000,
	"trillion": 1000000000000,
}

var (
	ErrUnexpectedToken               = errors.New("unexpected token")
	ErrUnexpectedSegmentedTimeFormat = errors.New("unexpected segmented time format")
)

type parser struct {
	input   string
	scanner *lexer.Scanner
}

func Parse(input string) (time.Duration, error) {
	p := parser{input: input, scanner: lexer.NewScanner(input)}

	d, err := p.parse()
	if err != nil {
		return 0, err
	}

	return time.Duration(d * float64(time.Second)), nil
}

func (p *parser) parse() (float64, error) {
	var duration float64

	for {
		token, offset, text := p.scanner.Scan()

		switch token {
		case lexer.EOF:
			return duration, nil

		case lexer.Error:
			return duration, fmt.Errorf("%w (offset: %v) (%v <)", ErrUnexpectedToken, offset, strconv.Quote(p.input[:offset+len(text)]))

		case lexer.Single, lexer.Direct, lexer.Ten, lexer.Big:
			unit := strings.ToLower(text)

			if duration == 0 {
				duration = durationMap[unit]
			} else {
				duration *= durationMap[unit]
			}

		case lexer.And:
			d, err := p.parse()
			duration += d

			if err != nil {
				return duration, err
			}

		case lexer.TimeUnit:
			unit := strings.ToLower(text)

			if duration == 0 {
				duration = durationMap[unit]
			} else {
				duration *= durationMap[unit]
			}

			d, err := p.parse()
			duration += d
			if err != nil {
				return duration, err
			}

		case lexer.Number:
			num, _ := strconv.ParseFloat(text, 64)
			duration += num

		case lexer.Time:
			var segments []float64
			for _, s := range strings.Split(text, ":") {
				segment, err := strconv.ParseFloat(s, 64)
				if err != nil || len(segments) > 3 {
					segments = nil
					break
				}

				segments = append(segments, segment)
			}

			switch len(segments) {
			case 2:
				duration += segments[0] * minute
				duration += segments[1] * second
			case 3:
				duration += segments[0] * hour
				duration += segments[1] * minute
				duration += segments[2] * second
			default:
				return duration, fmt.Errorf("%w (offset: %v) (%v <)", ErrUnexpectedSegmentedTimeFormat, offset, strconv.Quote(p.input[:offset+len(text)]))
			}

		default:
			return duration, io.ErrUnexpectedEOF
		}
	}
}
