# Duration

This duration library is intended to work with inputs (or at least, the most common inputs) used in [GitLab CI config durations](https://gitlab.com/gitlab-org/gitlab-chronic-duration).

## Example inputs

- 1:20
- 1:20.51
- 4:01:01
- 3 mins 4 sec
- three mins four sec
- 2 hrs 20 min
- 2h20min
- 6 mos 1 day
- 1 year 6 mos and 4.5d
- two hours and twenty minutes
- four hours and forty minutes
- 3 weeks and, 2 days
- 3 weeks, plus 2 days
- 3 weeks with 2 days
- 1 month
- 2 months
- 18 months
- 1 year 6 months
- day
- minute 30s
- sixteen hundred seconds
- thousand million seconds and 26 seconds

## Benchmarks

```sh
pkg: gitlab.com/jobd/ci/pkg/parser/duration
BenchmarkDuration-16    	13557374	      1774 ns/op	     184 B/op	       9 allocs/op
```
