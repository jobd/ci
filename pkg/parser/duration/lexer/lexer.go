package lexer

import (
	"strings"
	"unicode"
)

type Token struct {
	Type TokenType
}

type TokenType int

const (
	Error TokenType = iota
	EOF

	TimeUnit
	And

	Number
	Time

	Direct
	Single
	Ten
	Big
)

type Scanner struct {
	r *strings.Reader

	idx, size, offset int

	buf  strings.Builder
	next struct {
		token  TokenType
		text   string
		offset int
	}
}

func NewScanner(input string) *Scanner {
	s := &Scanner{r: strings.NewReader(input)}
	s.Scan()

	return s
}

func (s *Scanner) Scan() (TokenType, int, string) {
	token, offset, text := s.next.token, s.next.offset, s.next.text

	s.offset = offset
	s.next.token, s.next.offset, s.next.text = s.scan(), s.idx-s.buf.Len(), s.buf.String()

	return token, offset, text
}

func (s *Scanner) Peek() TokenType {
	return s.next.token
}

func (s *Scanner) Offset() int {
	return s.offset
}

func (s *Scanner) scan() TokenType {
	s.buf.Reset()
	r := s.read()

	switch {
	case isIgnored(r):
		for isIgnored(s.read()) {
		}
		s.unread()

		return s.scan()

	case isTime(r):
		s.unread()
		for isTime(s.peek()) {
			s.buf.WriteRune(s.read())
		}

		if strings.Contains(s.buf.String(), ":") {
			return Time
		}

		return Number

	case isWord(r):
		s.unread()
		for isWord(s.peek()) {
			s.buf.WriteRune(s.read())
		}

		switch strings.ToLower(s.buf.String()) {
		case "and", "plus", "with":
			return And
		case "one", "two", "three", "four", "five", "six", "seven", "eight", "nine":
			return Single
		case "zero", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen":
			return Direct
		case "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety":
			return Ten
		case "hundred", "thousand", "million", "billion", "trillion":
			return Big

		case "y", "w", "d", "h", "m", "s",
			"yr", "mo", "wk", "day", "hr", "min", "sec",
			"yrs", "mos", "wks", "days", "hrs", "mins", "secs",
			"year", "month", "week", "hour", "minute", "second",
			"years", "months", "weeks", "hours", "minutes", "seconds":
			return TimeUnit
		}

		return Error

	case r == rune(0):
		return EOF

	default:
		s.buf.WriteRune(r)
		return Error
	}
}

func (s *Scanner) read() rune {
	ch, i, err := s.r.ReadRune()
	if err != nil {
		return 0
	}

	s.size = i
	s.idx += i

	return ch
}

func (s *Scanner) unread() {
	s.r.UnreadRune()
	s.idx -= s.size
	s.size = 0
}

func (s *Scanner) peek() rune {
	r := s.read()
	if r != rune(0) {
		s.unread()
	}

	return r
}

func isIgnored(r rune) bool {
	return r == '-' || r == ',' || unicode.IsSpace(r)
}

func isTime(r rune) bool {
	return r == '.' || r == ':' || unicode.IsNumber(r)
}

func isWord(r rune) bool {
	return unicode.IsLetter(r)
}
