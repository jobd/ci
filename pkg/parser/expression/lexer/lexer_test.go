package lexer

import "testing"

type token struct {
	typ    TokenType
	offset int
	text   string
}

func TestLexer(t *testing.T) {
	tests := map[string]struct {
		expression string
		tokens     []token
	}{
		"returns single value": {
			expression: "$VARIABLE",
			tokens: []token{
				{Variable, 0, "$VARIABLE"},
			},
		},
		"does ignore whitespace characters": {
			expression: "\t$VARIABLE ",
			tokens: []token{
				{Variable, 1, "$VARIABLE"},
			},
		},
		"returns multiple values of the same token": {
			expression: "$VARIABLE1 $VARIABLE2",
			tokens: []token{
				{Variable, 0, "$VARIABLE1"},
				{Variable, 11, "$VARIABLE2"},
			},
		},
		"returns multiple values with different tokens": {
			expression: `$VARIABLE "text" "value"`,
			tokens: []token{
				{Variable, 0, "$VARIABLE"},
				{String, 12, "text"},
				{String, 19, "value"},
			},
		},
		"returns tokens and operators": {
			expression: `$VARIABLE == "text"`,
			tokens: []token{
				{Variable, 0, "$VARIABLE"},
				{Equal, 10, "=="},
				{String, 15, "text"},
			},
		},
		"raises error in case of finding unknown tokens": {
			expression: `$V1 123 $V2`,
			tokens: []token{
				{Variable, 0, "$V1"},
				{Error, 4, "123"},
			},
		},
		"complex expression #1": {
			expression: `$PRESENT_VARIABLE =~ /my var/ && $EMPTY_VARIABLE =~ /nope/`,
			tokens: []token{
				{Variable, 0, "$PRESENT_VARIABLE"},
				{Match, 18, "=~"},
				{Pattern, 21, "/my var/"},
				{And, 30, "&&"},
				{Variable, 33, "$EMPTY_VARIABLE"},
				{Match, 49, "=~"},
				{Pattern, 52, "/nope/"},
			},
		},
		"complex expression #2": {
			expression: `$EMPTY_VARIABLE == "" && $PRESENT_VARIABLE`,
			tokens: []token{
				{Variable, 0, "$EMPTY_VARIABLE"},
				{Equal, 16, "=="},
				{String, 21, ""},
				{And, 22, "&&"},
				{Variable, 25, "$PRESENT_VARIABLE"},
			},
		},
		"complex expression #3": {
			expression: `$EMPTY_VARIABLE == "" && $PRESENT_VARIABLE != "nope"`,
			tokens: []token{
				{Variable, 0, "$EMPTY_VARIABLE"},
				{Equal, 16, "=="},
				{String, 21, ""},
				{And, 22, "&&"},
				{Variable, 25, "$PRESENT_VARIABLE"},
				{NotEqual, 43, "!="},
				{String, 48, "nope"},
			},
		},
		"complex expression #4": {
			expression: `$PRESENT_VARIABLE && $EMPTY_VARIABLE`,
			tokens: []token{
				{Variable, 0, "$PRESENT_VARIABLE"},
				{And, 18, "&&"},
				{Variable, 21, "$EMPTY_VARIABLE"},
			},
		},
		"complex expression #5": {
			expression: `$PRESENT_VARIABLE =~ /my var/ || $EMPTY_VARIABLE =~ /nope/`,
			tokens: []token{
				{Variable, 0, "$PRESENT_VARIABLE"},
				{Match, 18, "=~"},
				{Pattern, 21, "/my var/"},
				{Or, 30, "||"},
				{Variable, 33, "$EMPTY_VARIABLE"},
				{Match, 49, "=~"},
				{Pattern, 52, "/nope/"},
			},
		},
		"complex expression #6": {
			expression: `$EMPTY_VARIABLE == "" || $PRESENT_VARIABLE`,
			tokens: []token{
				{Variable, 0, "$EMPTY_VARIABLE"},
				{Equal, 16, "=="},
				{String, 21, ""},
				{Or, 22, "||"},
				{Variable, 25, "$PRESENT_VARIABLE"},
			},
		},
		"complex expression #7": {
			expression: `$EMPTY_VARIABLE == "" || $PRESENT_VARIABLE != "nope"`,
			tokens: []token{
				{Variable, 0, "$EMPTY_VARIABLE"},
				{Equal, 16, "=="},
				{String, 21, ""},
				{Or, 22, "||"},
				{Variable, 25, "$PRESENT_VARIABLE"},
				{NotEqual, 43, "!="},
				{String, 48, "nope"},
			},
		},
		"complex expression #8": {
			expression: `$PRESENT_VARIABLE || $EMPTY_VARIABLE`,
			tokens: []token{
				{Variable, 0, "$PRESENT_VARIABLE"},
				{Or, 18, "||"},
				{Variable, 21, "$EMPTY_VARIABLE"},
			},
		},
		"complex expression #9": {
			expression: `$PRESENT_VARIABLE && null || $EMPTY_VARIABLE == ""`,
			tokens: []token{
				{Variable, 0, "$PRESENT_VARIABLE"},
				{And, 18, "&&"},
				{Null, 21, "null"},
				{Or, 26, "||"},
				{Variable, 29, "$EMPTY_VARIABLE"},
				{Equal, 45, "=="},
				{String, 50, ""},
			},
		},
		"complex expression with parentheses #1": {
			expression: `($PRESENT_VARIABLE =~ /my var/) && $EMPTY_VARIABLE =~ /nope/`,
			tokens: []token{
				{OpenParenthesis, 0, "("},
				{Variable, 1, "$PRESENT_VARIABLE"},
				{Match, 19, "=~"},
				{Pattern, 22, "/my var/"},
				{CloseParenthesis, 30, ")"},
				{And, 32, "&&"},
				{Variable, 35, "$EMPTY_VARIABLE"},
				{Match, 51, "=~"},
				{Pattern, 54, "/nope/"},
			},
		},
		"complex expression with parentheses #2": {
			expression: `$PRESENT_VARIABLE =~ /my var/ || ($EMPTY_VARIABLE =~ /nope/)`,
			tokens: []token{
				{Variable, 0, "$PRESENT_VARIABLE"},
				{Match, 18, "=~"},
				{Pattern, 21, "/my var/"},
				{Or, 30, "||"},
				{OpenParenthesis, 33, "("},
				{Variable, 34, "$EMPTY_VARIABLE"},
				{Match, 50, "=~"},
				{Pattern, 53, "/nope/"},
				{CloseParenthesis, 59, ")"},
			},
		},
		"complex expression with parentheses #3": {
			expression: `($PRESENT_VARIABLE && (null || $EMPTY_VARIABLE == ""))`,
			tokens: []token{
				{OpenParenthesis, 0, "("},
				{Variable, 1, "$PRESENT_VARIABLE"},
				{And, 19, "&&"},
				{OpenParenthesis, 22, "("},
				{Null, 23, "null"},
				{Or, 28, "||"},
				{Variable, 31, "$EMPTY_VARIABLE"},
				{Equal, 47, "=="},
				{String, 52, ""},
				{CloseParenthesis, 52, ")"},
				{CloseParenthesis, 53, ")"},
			},
		},
		"pattern with flags": {
			expression: `$VARIABLE !~ /foobar/imsU`,
			tokens: []token{
				{Variable, 0, "$VARIABLE"},
				{NotMatch, 10, "!~"},
				{Pattern, 13, "/foobar/imsU"},
			},
		},
		"escaped regexp pattern": {
			expression: `$VARIABLE !~ /foo\/bar/`,
			tokens: []token{
				{Variable, 0, "$VARIABLE"},
				{NotMatch, 10, "!~"},
				{Pattern, 13, `/foo\/bar/`},
			},
		},
		"regexp pattern with not terminating /": {
			expression: `$VARIABLE !~ /foo\/bar`,
			tokens: []token{
				{Variable, 0, "$VARIABLE"},
				{NotMatch, 10, "!~"},
				{Error, 13, `/foo\/bar`},
			},
		},
		"unsupported escaped string": {
			expression: `"foo\"bar"`,
			tokens: []token{
				{String, 2, "foo\\"},
				{Error, 6, "bar"},
			},
		},
		"unsupported operation #1": {
			expression: `$foo =* $bar`,
			tokens: []token{
				{Variable, 0, "$foo"},
				{Error, 5, "=*"},
			},
		},
		"unsupported operation #2": {
			expression: `$foo % $bar`,
			tokens: []token{
				{Variable, 0, "$foo"},
				{Error, 5, "%"},
			},
		},
	}

	for tn, tc := range tests {
		tc := tc
		t.Run(tn, func(t *testing.T) {
			tokens := readAllTokens(t, NewScanner(tc.expression))

			if len(tokens) != len(tc.tokens) {
				t.Errorf("incorrect tokens length (%v): got %d, expected %d", tokens, len(tokens), len(tc.tokens))
				return
			}

			for i := 0; i < len(tokens); i++ {
				got := tokens[i]
				expected := tc.tokens[i]

				if got != expected {
					t.Errorf("incorrect tokens: got %v, expected %v", tokens, tc.tokens)
				}
			}
		})
	}
}

func readAllTokens(t *testing.T, s *Scanner) []token {
	var tokens []token
	for {
		var tok token

		expectedTyp := s.Peek()

		tok.typ, tok.offset, tok.text = s.Scan()
		if tok.typ != EOF {
			tokens = append(tokens, tok)
		}

		if s.Offset() != tok.offset {
			t.Errorf("comparing current offsets: got %v, expected %v", s.Offset(), tok.offset)
		}

		if expectedTyp != tok.typ {
			t.Errorf("comparing peeked token: got %v, expected %v", tok.typ, expectedTyp)
		}

		if tok.typ == EOF || tok.typ == Error {
			break
		}
	}

	return tokens
}
