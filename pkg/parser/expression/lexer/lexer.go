package lexer

import (
	"strings"
	"unicode"
)

type Token struct {
	Type TokenType
}

type TokenType int

const (
	Error TokenType = iota
	EOF

	OpenParenthesis
	CloseParenthesis
	Variable
	String
	Pattern
	Null
	Equal
	Match
	NotEqual
	NotMatch
	And
	Or
)

type Scanner struct {
	r *strings.Reader

	idx, size, offset int

	buf  strings.Builder
	next struct {
		token  TokenType
		text   string
		offset int
	}
}

func NewScanner(expr string) *Scanner {
	s := &Scanner{r: strings.NewReader(expr)}
	s.Scan()

	return s
}

func (s *Scanner) Scan() (token TokenType, offset int, text string) {
	token, offset, text = s.next.token, s.next.offset, s.next.text

	s.offset = offset
	s.next.token, s.next.offset, s.next.text = s.scan(), s.idx-s.buf.Len(), s.buf.String()

	return token, offset, text
}

func (s *Scanner) Peek() TokenType {
	return s.next.token
}

func (s *Scanner) Offset() int {
	return s.offset
}

func (s *Scanner) scan() TokenType {
	s.buf.Reset()
	r := s.read()

	switch {
	case unicode.IsSpace(r):
		for unicode.IsSpace(s.read()) {
		}
		s.unread()

		return s.scan()

	case r == '(':
		s.buf.WriteRune(r)
		return OpenParenthesis

	case r == ')':
		s.buf.WriteRune(r)
		return CloseParenthesis

	case r == '$':
		s.buf.WriteRune(r)
		for isIdent(s.peek()) {
			s.buf.WriteRune(s.read())
		}
		return Variable

	case r == '"' || r == '\'':
		return s.scanAnchor(r, String)

	case r == '/':
		return s.scanPattern()

	case isIdent(r):
		s.unread()
		for isIdent(s.peek()) {
			s.buf.WriteRune(s.read())
		}

		ident := strings.ToLower(s.buf.String())
		if ident == "null" {
			return Null
		}

		return Error

	case r == '=' || r == '!' || r == '|' || r == '&':
		s.buf.WriteRune(r)
		s.buf.WriteRune(s.read())

		switch s.buf.String() {
		case "==":
			return Equal
		case "!=":
			return NotEqual
		case "=~":
			return Match
		case "!~":
			return NotMatch
		case "&&":
			return And
		case "||":
			return Or
		default:
			return Error
		}

	case r == rune(0):
		return EOF

	default:
		s.buf.WriteRune(r)
		return Error
	}
}

func (s *Scanner) read() rune {
	ch, i, err := s.r.ReadRune()
	if err != nil {
		return 0
	}

	s.size = i
	s.idx += i

	return ch
}

func (s *Scanner) unread() {
	s.r.UnreadRune()
	s.idx -= s.size
	s.size = 0
}

func (s *Scanner) peek() rune {
	r := s.read()
	if r != rune(0) {
		s.unread()
	}

	return r
}

func isIdent(r rune) bool {
	return r == '_' || unicode.IsLetter(r) || unicode.IsDigit(r)
}

func (s *Scanner) scanAnchor(terminating rune, token TokenType) TokenType {
	for {
		r := s.read()
		switch r {
		case rune(0):
			return Error

		case terminating:
			return token

		case '\\':
			p := s.peek()

			// GitLab's implementation doesn't support escaping in strings.
			// We use in patterns to find the terminating /
			if p == '/' {
				s.buf.WriteRune(r)
				s.buf.WriteRune(s.read())

				if p == terminating {
					continue
				}
			}
		}

		s.buf.WriteRune(r)
	}
}

func (s *Scanner) scanPattern() TokenType {
	s.buf.WriteRune('/')
	if s.scanAnchor('/', Pattern) != Pattern {
		return Error
	}
	s.buf.WriteRune('/')

	// flags
	for {
		r := s.peek()
		switch r {
		case 'i', 'm', 's', 'U':
			s.buf.WriteRune(s.read())

		default:
			return Pattern
		}
	}
}
