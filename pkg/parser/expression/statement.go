package expression

import "gitlab.com/jobd/ci/pkg/parser/expression/ast"

type Statement struct {
	input  string
	parser *Parser
	expr   ast.Expr
	err    error
}

func New(statement string) *Statement {
	return &Statement{
		input:  statement,
		parser: NewParser(statement),
	}
}

func (s *Statement) String() string {
	return s.input
}

func (s *Statement) parse() {
	if s.expr != nil || s.err != nil {
		return
	}
	s.expr, s.err = s.parser.Parse()
}

func (s *Statement) Evaluate(variables map[string]interface{}) (interface{}, error) {
	s.parse()
	if s.err != nil {
		return nil, s.err
	}

	return s.expr.Eval(variables)
}

func (s *Statement) Tree() (string, error) {
	s.parse()
	if s.err != nil {
		return "", s.err
	}

	return s.expr.String(), nil
}

func (s *Statement) Truthful(variables map[string]interface{}) bool {
	val, err := s.Evaluate(variables)
	if err != nil {
		return false
	}

	switch v := val.(type) {
	case bool:
		return v
	case string:
		return len(v) > 0
	}
	return false
}

func (s *Statement) Validate() error {
	s.parse()
	return s.err
}
