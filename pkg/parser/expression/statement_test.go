package expression

import (
	"reflect"
	"strings"
	"testing"
)

var testVariables = map[string]interface{}{
	"PRESENT_VARIABLE":   "my variable",
	"PATH_VARIABLE":      "a/path/variable/value",
	"FULL_PATH_VARIABLE": "/a/full/path/variable/value",
	"EMPTY_VARIABLE":     "",
}

func TestEvaluate(t *testing.T) {
	tests := []struct {
		expression string
		value      interface{}
	}{
		{`$PRESENT_VARIABLE == "my variable"`, true},
		{`"my variable" == $PRESENT_VARIABLE`, true},
		{`$PRESENT_VARIABLE == null`, false},
		{`$EMPTY_VARIABLE == null`, false},
		{`"" == $EMPTY_VARIABLE`, true},
		{`$EMPTY_VARIABLE`, ``},
		{`$UNDEFINED_VARIABLE == null`, true},
		{`null == $UNDEFINED_VARIABLE`, true},
		{`$PRESENT_VARIABLE`, `my variable`},
		{`$UNDEFINED_VARIABLE`, nil},
		{`$PRESENT_VARIABLE =~ /var.*e$/`, true},
		{`$PRESENT_VARIABLE =~ /va\r.*e$/`, false},
		{`$PRESENT_VARIABLE =~ /va\/r.*e$/`, false},
		{`$PRESENT_VARIABLE =~ /var.*e$/`, true},
		{`$PRESENT_VARIABLE =~ /^var.*/`, false},
		{`$EMPTY_VARIABLE =~ /var.*/`, false},
		{`$UNDEFINED_VARIABLE =~ /var.*/`, false},
		{`$PRESENT_VARIABLE =~ /VAR.*/i`, true},
		{`$PATH_VARIABLE =~ /path\/variable/`, true},
		{`$FULL_PATH_VARIABLE =~ /^\/a\/full\/path\/variable\/value$/`, true},
		{`$FULL_PATH_VARIABLE =~ /\/path\/variable\/value$/`, true},
		{`$PRESENT_VARIABLE != "my variable"`, false},
		{`"my variable" != $PRESENT_VARIABLE`, false},
		{`$PRESENT_VARIABLE != null`, true},
		{`$EMPTY_VARIABLE != null`, true},
		{`"" != $EMPTY_VARIABLE`, false},
		{`$UNDEFINED_VARIABLE != null`, false},
		{`null != $UNDEFINED_VARIABLE`, false},
		{"$PRESENT_VARIABLE !~ /var.*e$/", false},
		{"$PRESENT_VARIABLE !~ /^var.*/", true},
		{`$PRESENT_VARIABLE !~ /^v\ar.*/`, true},
		{`$PRESENT_VARIABLE !~ /^v\/ar.*/`, true},
		{"$EMPTY_VARIABLE !~ /var.*/", true},
		{"$UNDEFINED_VARIABLE !~ /var.*/", true},
		{"$PRESENT_VARIABLE !~ /VAR.*/i", false},

		{`$PRESENT_VARIABLE && "string"`, `string`},
		{`$PRESENT_VARIABLE && $PRESENT_VARIABLE`, `my variable`},
		{`$PRESENT_VARIABLE && $EMPTY_VARIABLE`, ``},
		{`$PRESENT_VARIABLE && null`, nil},
		{`"string" && $PRESENT_VARIABLE`, `my variable`},
		{`$EMPTY_VARIABLE && $PRESENT_VARIABLE`, `my variable`},
		{`null && $PRESENT_VARIABLE`, nil},
		{`$EMPTY_VARIABLE && "string"`, `string`},
		{`$EMPTY_VARIABLE && $EMPTY_VARIABLE`, ``},
		{`"string" && $EMPTY_VARIABLE`, ``},
		{`"string" && null`, nil},
		{`null && "string"`, nil},
		{`"string" && "string"`, `string`},
		{`null && null`, nil},

		{`$PRESENT_VARIABLE =~ /my var/ && $EMPTY_VARIABLE =~ /nope/`, false},
		{`$EMPTY_VARIABLE == "" && $PRESENT_VARIABLE`, `my variable`},
		{`$EMPTY_VARIABLE == "" && $PRESENT_VARIABLE != "nope"`, true},
		{`$PRESENT_VARIABLE && $EMPTY_VARIABLE`, ``},
		{`$PRESENT_VARIABLE && $UNDEFINED_VARIABLE`, nil},
		{`$UNDEFINED_VARIABLE && $EMPTY_VARIABLE`, nil},
		{`$UNDEFINED_VARIABLE && $PRESENT_VARIABLE`, nil},

		{`$FULL_PATH_VARIABLE =~ /^\/a\/full\/path\/variable\/value$/ && $PATH_VARIABLE =~ /path\/variable/`, true},
		{`$FULL_PATH_VARIABLE =~ /^\/a\/bad\/path\/variable\/value$/ && $PATH_VARIABLE =~ /path\/variable/`, false},
		{`$FULL_PATH_VARIABLE =~ /^\/a\/full\/path\/variable\/value$/ && $PATH_VARIABLE =~ /bad\/path\/variable/`, false},
		{`$FULL_PATH_VARIABLE =~ /^\/a\/bad\/path\/variable\/value$/ && $PATH_VARIABLE =~ /bad\/path\/variable/`, false},

		{`$FULL_PATH_VARIABLE =~ /^\/a\/full\/path\/variable\/value$/ || $PATH_VARIABLE =~ /path\/variable/`, true},
		{`$FULL_PATH_VARIABLE =~ /^\/a\/bad\/path\/variable\/value$/ || $PATH_VARIABLE =~ /path\/variable/`, true},
		{`$FULL_PATH_VARIABLE =~ /^\/a\/full\/path\/variable\/value$/ || $PATH_VARIABLE =~ /bad\/path\/variable/`, true},
		{`$FULL_PATH_VARIABLE =~ /^\/a\/bad\/path\/variable\/value$/ || $PATH_VARIABLE =~ /bad\/path\/variable/`, false},

		{`$PRESENT_VARIABLE =~ /my var/ || $EMPTY_VARIABLE =~ /nope/`, true},
		{`$EMPTY_VARIABLE == "" || $PRESENT_VARIABLE`, true},
		{`$PRESENT_VARIABLE != "nope" || $EMPTY_VARIABLE == ""`, true},

		{`$PRESENT_VARIABLE && null || $EMPTY_VARIABLE == ""`, true},
		{`$PRESENT_VARIABLE || $UNDEFINED_VARIABLE`, `my variable`},
		{`$UNDEFINED_VARIABLE || $PRESENT_VARIABLE`, `my variable`},
		{`$UNDEFINED_VARIABLE == null || $PRESENT_VARIABLE`, true},
		{`$PRESENT_VARIABLE || $UNDEFINED_VARIABLE == null`, `my variable`},

		{`($PRESENT_VARIABLE)`, `my variable`},
		{`(($PRESENT_VARIABLE))`, `my variable`},
		{`(($PRESENT_VARIABLE && null) || $EMPTY_VARIABLE == "")`, true},
		{`($PRESENT_VARIABLE) && (null || $EMPTY_VARIABLE == "")`, true},
		{`("string" || "test") == "string"`, true},
		{`(null || ("test" == "string"))`, false},
		{`("string" == ("test" && "string"))`, true},
		{`("string" == ("test" || "string"))`, false},
		{`("string" == "test" || "string")`, "string"},
		{`("string" == ("string" || (("1" == "1") && ("2" == "3"))))`, true},
	}

	for _, tc := range tests {
		tc := tc
		t.Run(tc.expression, func(t *testing.T) {
			stmt := New(tc.expression)

			if tc.expression != stmt.String() {
				t.Errorf("got %v, expected %v", stmt.String(), tc.expression)
			}

			if err := stmt.Validate(); err != nil {
				t.Error(err)
			}

			result, err := stmt.Evaluate(testVariables)
			if err != nil {
				t.Error(err)
			}

			if !reflect.DeepEqual(result, tc.value) {
				t.Errorf("got %v, expected %v", result, tc.value)
			}
		})
	}
}

func TestTruthful(t *testing.T) {
	tests := []struct {
		expression string
		truthful   bool
	}{
		{`$PRESENT_VARIABLE == "my variable"`, true},
		{`$PRESENT_VARIABLE == 'no match'`, false},
		{`$UNDEFINED_VARIABLE == null`, true},
		{`$PRESENT_VARIABLE`, true},
		{`$UNDEFINED_VARIABLE`, false},
		{`$EMPTY_VARIABLE || $PRESENT_VARIABLE`, false},
		{`$INVALID = 1`, false},
		{`$PRESENT_VARIABLE =~ /var.*/`, true},
		{`$UNDEFINED_VARIABLE =~ /var.*/`, false},
		{`$PRESENT_VARIABLE !~ /var.*/`, false},
		{`$UNDEFINED_VARIABLE !~ /var.*/`, true},
	}

	for _, tc := range tests {
		tc := tc
		t.Run(tc.expression, func(t *testing.T) {
			stmt := New(tc.expression)

			truthful := stmt.Truthful(testVariables)
			if tc.truthful != truthful {
				t.Errorf("got %v, expected %v", truthful, tc.truthful)
			}
		})
	}
}

func TestEvaluateInvalid(t *testing.T) {
	tests := []struct {
		expression string
		err        string
	}{
		{``, ``},
		{`string != true`, `unexpected token in expression (offset: 0) ("string" <)`},
		{`"foo'`, `unexpected token in expression (offset: 1) ("\"foo'" <)`},
		{`'bar"`, "a"},
		{`$PRESENT_VARIABLE == == "my variable"`, `unexpected token in expression (offset: 21) ("$PRESENT_VARIABLE == ==" <)`},
		{`$PRESENT_VARIABLE == || "my variable"`, `unexpected token in expression (offset: 21) ("$PRESENT_VARIABLE == ||" <)`},
		{`$PRESENT_VARIABLE =~ /\/`, `unexpected token in expression (offset: 21) ("$PRESENT_VARIABLE =~ /\\/" <)`},
		{`$PRESENT_VARIABLE =~ /(?foobar)/`, `invalid regexp pattern in expression (offset: 21) ("$PRESENT_VARIABLE =~ /(?foobar)/" <)`},
	}

	for _, tc := range tests {
		tc := tc
		t.Run(tc.expression, func(t *testing.T) {
			stmt := New(tc.expression)

			err := stmt.Validate()
			if err == nil || !strings.Contains(err.Error(), tc.err) {
				t.Errorf("got %q, expected %q", err, tc.err)
			}

			_, err = stmt.Tree()
			if err == nil {
				t.Errorf("got %q, expected %q", err, tc.err)
			}
		})
	}
}

func TestEvaluateTree(t *testing.T) {
	expected := `equals($VAR1, "123")`

	stmt := New(`$VAR1 == "123"`)
	tree, err := stmt.Tree()
	if err != nil {
		t.Error(err)
	}

	if tree != `equals($VAR1, "123")` {
		t.Errorf("invalid tree: got %s, expected %s", tree, expected)
	}
}
