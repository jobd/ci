package ast

import (
	"fmt"
)

type Value struct {
	Val interface{}
}

func (expr *Value) Eval(data map[string]interface{}) (interface{}, error) {
	return expr.Val, nil
}

func (expr *Value) String() string {
	return fmt.Sprintf("%q", expr.Val)
}
