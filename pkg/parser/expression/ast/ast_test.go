package ast

import (
	"regexp"
	"testing"

	"gitlab.com/jobd/ci/pkg/parser/expression/lexer"
)

func TestAST(t *testing.T) {
	tree := &Binary{
		Op: Operator(lexer.And),
		LHS: &Binary{
			Op: Operator(lexer.Equal),
			LHS: &Binary{
				Op: Operator(lexer.And),
				LHS: &Binary{
					Op:  Operator(lexer.NotEqual),
					LHS: &Variable{Val: "VAR1"},
					RHS: &Variable{Val: "VAR2"},
				},
				RHS: &Binary{
					Op:  Operator(lexer.Equal),
					LHS: &Value{Val: "hello"},
					RHS: &Variable{Val: "VAR2"},
				},
			},
			RHS: &Parentheses{
				Expr: &Binary{
					Op: Operator(lexer.Or),
					LHS: &Binary{
						Op:  Operator(lexer.Match),
						LHS: &Value{Val: "hello"},
						RHS: &Pattern{Val: regexp.MustCompile(".*")},
					},
					RHS: &Binary{
						Op:  Operator(lexer.NotMatch),
						LHS: &Value{Val: "hello"},
						RHS: &Pattern{Val: regexp.MustCompile(".*")},
					},
				},
			},
		},
		RHS: &Variable{Val: "VAR1"},
	}

	_, err := tree.Eval(map[string]interface{}{
		"VAR1": "hello",
	})
	if err != nil {
		t.Error(err)
	}

	expected := `and(equals(and(not_equals($VAR1, $VAR2), equals("hello", $VAR2)), (or(matchs("hello", /.*/), not_matches("hello", /.*/)))), $VAR1)`
	if tree.String() != expected {
		t.Errorf("invalid tree string: got %s, expected %s", tree.String(), expected)
	}
}

func TestOperator(t *testing.T) {
	operators := Operator(lexer.And).IsOperator() &&
		Operator(lexer.Or).IsOperator() &&
		Operator(lexer.Equal).IsOperator() &&
		Operator(lexer.NotEqual).IsOperator() &&
		Operator(lexer.Match).IsOperator() &&
		Operator(lexer.NotMatch).IsOperator()

	if !operators {
		t.Error("isOperator() returned false for an operator")
	}

	precedence := Operator(lexer.And).Precedence() == 2 &&
		Operator(lexer.Or).Precedence() == 1 &&
		Operator(lexer.Equal).Precedence() == 3 &&
		Operator(lexer.NotEqual).Precedence() == 3 &&
		Operator(lexer.Match).Precedence() == 3 &&
		Operator(lexer.NotMatch).Precedence() == 3

	if !precedence {
		t.Error("Precedence() returned incorrect operator precedence")
	}

	if Operator(lexer.Error).IsOperator() ||
		Operator(lexer.Error).Precedence() > 0 ||
		Operator(lexer.Error).String() != "unknown" {
		t.Error("expected unsupported operator")
	}
}

func TestTruthy(t *testing.T) {
	if !(truthy(true) &&
		truthy("") &&
		truthy("hello") &&
		truthy(regexp.MustCompile(".*"))) {
		t.Error("truthy values should be true")
	}

	if truthy(false) || truthy(nil) {
		t.Error("truthy values should be false")
	}
}
