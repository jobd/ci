package ast

type Parentheses struct {
	Expr Expr
}

func (expr *Parentheses) Eval(data map[string]interface{}) (interface{}, error) {
	return expr.Expr.Eval(data)
}

func (expr *Parentheses) String() string {
	return "(" + expr.Expr.String() + ")"
}
