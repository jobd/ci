package ast

import (
	"regexp"

	"gitlab.com/jobd/ci/pkg/parser/expression/lexer"
)

type Binary struct {
	Op  Operator
	LHS Expr
	RHS Expr
}

func (expr *Binary) Eval(data map[string]interface{}) (val interface{}, err error) {
	lhs, _ := expr.LHS.Eval(data)
	rhs, _ := expr.RHS.Eval(data)

	return op(lhs, expr.Op, rhs), nil
}

func (expr *Binary) String() string {
	return expr.Op.String() + "(" + expr.LHS.String() + ", " + expr.RHS.String() + ")"
}

func op(lhs interface{}, op Operator, rhs interface{}) (val interface{}) {
	switch lexer.TokenType(op) {
	case lexer.Or:
		if truthy(lhs) {
			return lhs
		}
		return rhs

	case lexer.And:
		if truthy(lhs) {
			return rhs
		}
		return lhs

	case lexer.Equal:
		return lhs == rhs

	case lexer.NotEqual:
		return lhs != rhs

	case lexer.Match:
		return match(lhs, rhs)

	case lexer.NotMatch:
		return !match(lhs, rhs)
	}

	return nil
}

func truthy(val interface{}) bool {
	switch val := val.(type) {
	case bool:
		return val
	case string:
		return true
	case *regexp.Regexp:
		return true
	default:
		return false
	}
}

func match(lhs, rhs interface{}) bool {
	pattern, ok := rhs.(*regexp.Regexp)
	if !ok {
		return false
	}

	s, ok := lhs.(string)
	if !ok {
		return false
	}

	return pattern.MatchString(s)
}
