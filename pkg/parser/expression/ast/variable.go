package ast

type Variable struct {
	Val string
}

func (expr *Variable) Eval(data map[string]interface{}) (interface{}, error) {
	return data[expr.Val], nil
}

func (expr *Variable) String() string {
	return "$" + expr.Val
}
