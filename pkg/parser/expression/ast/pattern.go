package ast

import "regexp"

type Pattern struct {
	Val *regexp.Regexp
}

func (expr *Pattern) Eval(data map[string]interface{}) (interface{}, error) {
	return expr.Val, nil
}

func (expr *Pattern) String() string {
	return "/" + expr.Val.String() + "/"
}
