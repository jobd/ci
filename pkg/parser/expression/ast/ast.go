package ast

type Expr interface {
	Eval(data map[string]interface{}) (interface{}, error)
	String() string
}
