package ast

import "gitlab.com/jobd/ci/pkg/parser/expression/lexer"

type Operator lexer.TokenType

func (o Operator) Precedence() int {
	switch lexer.TokenType(o) {
	case lexer.Or:
		return 1
	case lexer.And:
		return 2
	case lexer.Equal, lexer.NotEqual, lexer.Match, lexer.NotMatch:
		return 3
	}
	return 0
}

func (o Operator) IsOperator() bool {
	switch lexer.TokenType(o) {
	case lexer.And, lexer.Or, lexer.Equal, lexer.NotEqual, lexer.Match, lexer.NotMatch:
		return true
	}

	return false
}

func (o Operator) String() string {
	switch lexer.TokenType(o) {
	case lexer.And:
		return "and"
	case lexer.Or:
		return "or"
	case lexer.Equal:
		return "equals"
	case lexer.NotEqual:
		return "not_equals"
	case lexer.Match:
		return "matchs"
	case lexer.NotMatch:
		return "not_matches"
	}

	return "unknown"
}
