package expression

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/jobd/ci/pkg/parser/expression/ast"
	"gitlab.com/jobd/ci/pkg/parser/expression/lexer"
)

type Parser struct {
	s     *lexer.Scanner
	input string
}

func NewParser(input string) *Parser {
	return &Parser{s: lexer.NewScanner(input), input: input}
}

func (p *Parser) Parse() (expr ast.Expr, err error) {
	defer func() {
		if r := recover(); r != nil {
			err = fmt.Errorf("%v", r)
		}
	}()

	expr = p.Expression(1)
	p.match(lexer.EOF)

	return expr, err
}

func (p *Parser) Expression(precedence int) ast.Expr {
	lhs := p.UnaryExpression()

	for {
		op := ast.Operator(p.s.Peek())
		if op.IsOperator() && op.Precedence() >= precedence {
			p.match(lexer.TokenType(op))
		} else {
			break
		}

		rhs := p.Expression(op.Precedence())
		lhs = &ast.Binary{Op: op, LHS: lhs, RHS: rhs}
	}

	return lhs
}

func (p *Parser) UnaryExpression() ast.Expr {
	token := p.s.Peek()

	switch token {
	case lexer.OpenParenthesis:
		p.match(lexer.OpenParenthesis)
		paren := &ast.Parentheses{Expr: p.Expression(1)}
		p.match(lexer.CloseParenthesis)

		return paren

	case lexer.String:
		return &ast.Value{Val: p.match(lexer.String)}

	case lexer.Variable:
		return &ast.Variable{Val: p.match(lexer.Variable)[1:]}

	case lexer.Null:
		p.match(lexer.Null)
		return &ast.Value{Val: nil}

	case lexer.Pattern:
		var (
			raw     = p.match(lexer.Pattern)
			end     = strings.LastIndex(raw, "/")
			pattern = strings.Trim(raw[:end], "/")
			flags   = raw[end+1:]
		)

		re, err := regexp.Compile("(?" + flags + ")" + pattern)
		if err != nil {
			panic(p.error("invalid regexp pattern in expression", p.s.Offset(), len(raw)))
		}

		return &ast.Pattern{Val: re}

	default:
		_, offset, text := p.s.Scan()
		panic(p.error("unexpected token in expression", offset, len(text)))
	}
}

func (p *Parser) error(str string, offset, size int) string {
	return fmt.Sprintf("%v (offset: %v) (%v <)", str, offset, strconv.Quote(p.input[:offset+size]))
}

func (p *Parser) match(token lexer.TokenType) string {
	t, offset, text := p.s.Scan()
	if t != token {
		panic(p.error("unexpected token", offset, len(text)))
	}

	return text
}
